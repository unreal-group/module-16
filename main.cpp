#include <ctime>
#include <iostream>

int currentDate() {
    struct tm newtime;
    std::time_t t = std::time(nullptr);
    localtime_s(&newtime, &t);
    return newtime.tm_mday;
}

int main() {
    const int size = 5;
    int array[size][size];

    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            array[i][j] = i + j;
            std::cout << array[i][j] << " ";
        }
        std::cout << std::endl;
    }

    std::cout << "======================" << std::endl;

    int index = currentDate() % size;

    for (int j = 0; j < size; j++) {
        std::cout << array[index][j] << " ";
    }
}
